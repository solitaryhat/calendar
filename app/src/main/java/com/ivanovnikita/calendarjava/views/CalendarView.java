package com.ivanovnikita.calendarjava.views;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.adapters.CalendarAdapter;
import com.ivanovnikita.calendarjava.interfaces.EventHandler;
import com.ivanovnikita.calendarjava.utils.DateUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class CalendarView extends LinearLayout {

    private static final int DAYS_COUNT = 42;
    private static final String DATE_FORMAT = "MMM yyyy";

    private ImageView mPrevButton;
    private ImageView mNextButton;
    private TextView mDateText;
    private GridView mGrid;

    private Calendar mCurrentDate;

    private EventHandler mEventHandler;

    private HashSet<Date> mEventDays;

    private Date mTouchedDate;

    private CalendarAdapter mCalendarAdapter;


    public CalendarView(Context context)
    {
        super(context);
        mCurrentDate = Calendar.getInstance();
        mEventDays = new HashSet<>();
    }

    public CalendarView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        mCurrentDate = Calendar.getInstance();
        mEventDays = new HashSet<>();
        mTouchedDate = new Date();
        initControls(context, attrs);
    }

    public CalendarView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        mCurrentDate = Calendar.getInstance();
        mEventDays = new HashSet<>();
        mTouchedDate = new Date();
        initControls(context, attrs);
    }

    private void initControls(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.calendar_control, this);

        assignUi();
        assignClickHandlers();

    }

    private void assignUi() {
        mPrevButton = (ImageView) findViewById(R.id.calendar_prev_button);
        mNextButton = (ImageView) findViewById(R.id.calendar_next_button);
        mDateText = (TextView) findViewById(R.id.month_text_view);
        mGrid = (GridView) findViewById(R.id.grid_view);
    }

    private void assignClickHandlers() {
        mNextButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentDate.add(Calendar.MONTH, 1);
                updateCalendar();
            }
        });

        mPrevButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentDate.add(Calendar.MONTH, -1);
                updateCalendar();
            }
        });

        mGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (mEventHandler != null) {
                    mTouchedDate = (Date)parent.getItemAtPosition(position);
                    mEventHandler.onDayPress(mTouchedDate);
                    mCalendarAdapter.setSelection(mTouchedDate);
                }
            }
        });
    }

    public void updateCalendar() {
        ArrayList<Date> cells = new ArrayList<>();
        Calendar calendar = (Calendar)mCurrentDate.clone();

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

        while (cells.size() < DAYS_COUNT)
        {
            cells.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        mCalendarAdapter = new CalendarAdapter(
                getContext(),
                cells,
                getEventDaysInCurrentMonth(),
                mCurrentDate.getTime(),
                mTouchedDate);

        mGrid.setAdapter(mCalendarAdapter);

        mDateText.setText(new SimpleDateFormat(DATE_FORMAT).format(mCurrentDate.getTime()));
    }

    public void setEventDays(HashSet<Date> events) {
        mEventDays = events;
    }

    private HashSet<Date> getEventDaysInCurrentMonth() {
        HashSet<Date> dates = new HashSet<>();
        for (Date date : mEventDays) {
            if (DateUtils.sameMonth(date, mCurrentDate.getTime())) {
                dates.add(date);
            }
        }
        return  dates;
    }

    public void setEventHandler(EventHandler handler) {
        mEventHandler = handler;
    }
}

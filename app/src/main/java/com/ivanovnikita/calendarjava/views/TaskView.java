package com.ivanovnikita.calendarjava.views;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.activities.MainActivity;
import com.ivanovnikita.calendarjava.data.Task;
import com.ivanovnikita.calendarjava.utils.DateUtils;
import com.ivanovnikita.calendarjava.utils.SQLiteORM;

import java.util.Date;

/**
 * Created by test on 22.05.2016.
 */
public class TaskView extends CoordinatorLayout{

    private Task mTask;
    private EditText mTitleEditText;
    private EditText mDescriptionEditText;
    private Toolbar mToolbar;

    private FloatingActionButton mFabSave;
    private FloatingActionButton mFabEdit;
    private FloatingActionButton mFabDelete;

    private Date mDate;

    private Boolean mIsExistingTask;

    public TaskView(Context context)
    {
        super(context);
    }

    public TaskView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        initControls(context, attrs);
    }

    public TaskView(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        initControls(context, attrs);
    }

    private void initControls(Context context, AttributeSet attrs) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.task_control, this);

        assingUi();
    }

    private void assingUi() {
        mTitleEditText = (EditText) findViewById(R.id.new_task_title);
        mDescriptionEditText = (EditText) findViewById(R.id.new_task_description);
        initFabs();
    }

    private void initFabs() {
        mFabEdit = (FloatingActionButton) findViewById(R.id.fab_task_edit);
        mFabEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toEditMode();
            }
        });

        mFabSave = (FloatingActionButton) findViewById(R.id.fab_task_save);
        mFabSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveTask()) {
                    toViewMode();
                }
            }
        });

        mFabDelete = (FloatingActionButton) findViewById(R.id.fab_task_delete);
        mFabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteTask();
                MainActivity.hideTaskView();
            }
        });
    }

    public void setInfo(String dateAsString, Task task, Boolean isEditMode) {
        mTitleEditText.setText("");
        mDescriptionEditText.setText("");

        mDate = DateUtils.fromString(dateAsString);
        if (isEditMode) {
            mIsExistingTask = false;
            toEditMode();
        } else {
            mTask = task;
            mIsExistingTask = true;
            fetchTaskInfo();
            toViewMode();
        }
    }

    private void toEditMode() {
        mTitleEditText.setEnabled(true);
        mDescriptionEditText.setEnabled(true);
        mFabEdit.hide();
        if (!mIsExistingTask) {
            mFabDelete.hide();
        }
        mFabSave.show();
    }

    private void toViewMode() {
        mTitleEditText.setEnabled(false);
        mDescriptionEditText.setEnabled(false);
        mFabSave.hide();
        mFabEdit.show();
        mFabDelete.show();
    }

    private void fetchTaskInfo() {
        mTitleEditText.setText(mTask.getTitle());
        mDescriptionEditText.setText(mTask.getDescription());
    }

    private Boolean saveTask() {
        if (checkFields()) {
            SQLiteORM sqLiteORM= new SQLiteORM(getContext());
            Task newTask = new Task(mTitleEditText.getText().toString(),
                    mDescriptionEditText.getText().toString(),
                    mDate);
            if (mIsExistingTask) {
                sqLiteORM.updateTask(mTask, newTask);
            } else {
                sqLiteORM.addTaskItem(newTask);
            }
            mIsExistingTask = true;
            mTask = newTask;
            return true;
        }
        return false;
    }

    private Boolean checkFields() {
        if (mTitleEditText.getText().toString().equals("")) {
            mTitleEditText.setError(getContext().getString(R.string.error_title_not_filled));
            return false;
        }
        return true;
    }

    private void deleteTask() {
        if (mIsExistingTask) {
            SQLiteORM sqLiteORM= new SQLiteORM(getContext());
            sqLiteORM.deleteTask(mTask);
        }
    }
}

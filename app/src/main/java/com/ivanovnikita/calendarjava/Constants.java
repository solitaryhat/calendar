package com.ivanovnikita.calendarjava;

/**
 * Created by test on 04.05.2016.
 */
public class Constants {

    public static final String TASK = "task";
    public static final String EDIT_MODE = "isEdit";

    public static final String SP_NAME = "shared_prefs";
    public static final String SP_DATE = "spDate";

    public static final String DATE = "date";
    public static final String TASK_ID = "id";
}

package com.ivanovnikita.calendarjava.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;

public class CalendarAdapter extends ArrayAdapter<Date> {
    private HashSet<Date> mEventDays;
    private LayoutInflater mInflater;
    private Context mContext;
    private Date mCalendarDate;
    private Date mTouchedDate;

    public CalendarAdapter(Context context, ArrayList<Date> days, HashSet<Date> eventDays, Date calendarDate, Date touchedDate) {
        super(context, R.layout.calendar_day_control, days);
        mEventDays = eventDays;
        mInflater = mInflater.from(context);
        mContext = context;
        mCalendarDate = calendarDate;
        mTouchedDate = touchedDate;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Date date = getItem(position);
        int day = date.getDate();
        int month = date.getMonth();
        int year = date.getYear();

        Date today = new Date();

        if (view == null)
            view = mInflater.inflate(R.layout.calendar_day_control, parent, false);

        view.setBackgroundColor(0);
        ((TextView)view).setTypeface(null, Typeface.NORMAL);
        ((TextView)view).setTextColor(Color.BLACK);

        Boolean haveEvents = false;
        if (mEventDays != null) {
            for (Date eventDate : mEventDays) {
                if (DateUtils.sameDate(eventDate, date)) {
                    view.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                    haveEvents = true;
                    break;
                }
            }
        }
        if (DateUtils.sameDate(mTouchedDate, date)) {
            view.setBackgroundColor(mContext.getResources().getColor(R.color.colorPrimary));
            ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.white));
        }

        if (month != mCalendarDate.getMonth() || year != mCalendarDate.getYear()) {
            ((TextView)view).setTextColor(mContext.getResources().getColor(R.color.greyed_out));
        }
        else if (DateUtils.sameDate(date, today)) {
            ((TextView)view).setTypeface(null, Typeface.BOLD);
            if (haveEvents) {
                ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.white));
            } else {
                ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.today));
            }

        }

        ((TextView)view).setText(String.valueOf(date.getDate()));

        return view;
    }

    public void setSelection(Date selectedDate) {
        mTouchedDate = selectedDate;
        notifyDataSetChanged();
    }
}

package com.ivanovnikita.calendarjava.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.data.Task;

import java.util.List;

/**
 * Created by test on 19.05.2016.
 */
public class TaskRecyclerAdapter extends  RecyclerView.Adapter<TaskRecyclerAdapter.ViewHolder> {

    private List<Task> mDataset;
    private View.OnClickListener mOnClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View mView;

        public ViewHolder(View v) {
            super(v);
            mView = v;
        }

        public void setOnClickListenter(View.OnClickListener onClickListenter) {
            mView.findViewById(R.id.card_view).setOnClickListener(onClickListenter);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TaskRecyclerAdapter(List<Task> myDataset) {
        mDataset = myDataset;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TaskRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        viewHolder.setOnClickListenter(mOnClickListener);
        return viewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TextView taskTitle = (TextView) holder.mView.findViewById(R.id.task_title);
        taskTitle.setText(mDataset.get(position).getTitle());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}

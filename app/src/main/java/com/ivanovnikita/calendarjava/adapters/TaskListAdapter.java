package com.ivanovnikita.calendarjava.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.data.Task;

import java.util.List;

public class TaskListAdapter extends BaseAdapter{

    private List<Task> mData;
    private static LayoutInflater mInflater;

    public TaskListAdapter(Context context, List<Task> data) {
        mData = data;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View root = convertView;
        if (root == null) {
            root = mInflater.inflate(R.layout.task_list_item, parent, false);
        }
        TextView titleView = (TextView) root.findViewById(R.id.task_title);
        titleView.setText(mData.get(position).getTitle());
        return root;
    }

    @Override
    public Task getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

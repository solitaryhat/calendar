package com.ivanovnikita.calendarjava.interfaces;

/**
 * Created by test on 07.05.2016.
 */
public interface LifecycleHandler {
    void afterCreate();
}

package com.ivanovnikita.calendarjava.interfaces;

import java.util.Date;

public interface EventHandler {
    void onDayPress(Date date);
}

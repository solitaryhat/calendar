package com.ivanovnikita.calendarjava.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Pair;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.adapters.ViewPagerAdapter;
import com.ivanovnikita.calendarjava.data.Task;
import com.ivanovnikita.calendarjava.fragments.MonthFragment;
import com.ivanovnikita.calendarjava.fragments.TodayFragment;
import com.ivanovnikita.calendarjava.views.TaskView;

public class MainActivity extends AppCompatActivity  {

    private static TaskView mTaskView;
    private static Pair<Integer, Integer> mPoint;

    private static Boolean mIsShowingTask;

    private static MonthFragment mMonthFragment;
    private static TodayFragment mTodayFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        setupViewPager(viewPager);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        mTaskView = (TaskView)findViewById(R.id.task_view_root);

        mTaskView.setVisibility(View.GONE);
        mIsShowingTask = false;
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        mMonthFragment = new MonthFragment();
        mTodayFragment = new TodayFragment();
        adapter.addFragment(mMonthFragment, getString(R.string.month));
        adapter.addFragment(mTodayFragment, getString(R.string.today));
        viewPager.setAdapter(adapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

    }

    public static void showTaskView(Pair<Integer, Integer> point, String dateString, Task task, Boolean isEditMode) {
        mIsShowingTask = true;
        mPoint = point;
        int radius = mTaskView.getHeight();
        Animator animator = ViewAnimationUtils.createCircularReveal(mTaskView, mPoint.first, mPoint.second, 0, radius);
        mTaskView.setInfo(dateString, task, isEditMode);
        mTaskView.setVisibility(View.VISIBLE);

        animator.start();
    }

    public static void hideTaskView() {
        int radius = mTaskView.getHeight();
        Animator animator = ViewAnimationUtils.createCircularReveal(mTaskView, mPoint.first, mPoint.second, radius, 0);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mTaskView.setVisibility(View.GONE);
                mIsShowingTask = false;
                mMonthFragment.onResume();
                mTodayFragment.onResume();
            }
        });
        animator.start();
    }

    @Override
    public void onBackPressed() {
        if (mIsShowingTask) {
            hideTaskView();
        } else {
            super.onBackPressed();
        }

    }
}

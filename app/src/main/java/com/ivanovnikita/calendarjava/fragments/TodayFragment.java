package com.ivanovnikita.calendarjava.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.data.Task;
import com.ivanovnikita.calendarjava.interfaces.LifecycleHandler;
import com.ivanovnikita.calendarjava.utils.SQLiteORM;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class TodayFragment extends Fragment {

    private View mRootView;
    private static final String DATE_FORMAT = "dd MMMM";
    private static final String DATE_FORMAT_WEEK = "EEEE";

    private TasksFragment mTasksFragment;
    private Boolean mIsTaskFragmentCreated = false;

    public TodayFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_today, container, false);

        initListView();
        initDate();

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mIsTaskFragmentCreated){
            updateTaskListByDate();
        }
    }

    private void initDate() {
        TextView dayMonthTextView = (TextView) mRootView.findViewById(R.id.day_month_textview);
        TextView dayOfWeekTextView = (TextView) mRootView.findViewById(R.id.day_of_week_textview);

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date today = Calendar.getInstance().getTime();
        String dayMonth = sdf.format(today);
        sdf.applyPattern(DATE_FORMAT_WEEK);
        String dayOfWeek = sdf.format(today);
        dayMonthTextView.setText(dayMonth);
        dayOfWeekTextView.setText(dayOfWeek);
    }

    private void initListView() {

        SQLiteORM orm = new SQLiteORM(getContext());
        List<Task> tasksList = new ArrayList<>(orm.getTasksItemsByDay(new Date()));

        if (mRootView.findViewById(R.id.fragment_container_today) != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            mTasksFragment = new TasksFragment();
            mTasksFragment.setTasks(tasksList);
            mTasksFragment.setLifecycleHandler(new LifecycleHandler() {
                @Override
                public void afterCreate() {
                    mIsTaskFragmentCreated = true;
                }
            });

            fragmentTransaction.add(R.id.fragment_container_today, mTasksFragment);
            fragmentTransaction.commit();

        }
    }

    private void updateTaskListByDate() {
        SQLiteORM db = new SQLiteORM(getContext());
        List<Task> tasks = db.getTasksItemsByDay(new Date());
        mTasksFragment.updateList(tasks, new Date());
    }

}

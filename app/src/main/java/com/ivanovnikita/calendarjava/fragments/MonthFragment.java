package com.ivanovnikita.calendarjava.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.data.Task;
import com.ivanovnikita.calendarjava.interfaces.EventHandler;
import com.ivanovnikita.calendarjava.interfaces.LifecycleHandler;
import com.ivanovnikita.calendarjava.interfaces.LoaderHandler;
import com.ivanovnikita.calendarjava.utils.HolidaysLoadHelper;
import com.ivanovnikita.calendarjava.utils.SQLiteORM;
import com.ivanovnikita.calendarjava.views.CalendarView;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class MonthFragment extends Fragment implements EventHandler, LoaderHandler {

    private View mRootView;
    private SQLiteORM mDb;
    private TasksFragment mTasksFragment;
    private CalendarView mCalendarView;
    private Date mSelectedDate;
    private Boolean mIsTaskFragmentCreated = false;

    public MonthFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDb = new SQLiteORM(getContext());
        mSelectedDate = new Date();
        loadHolidays();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_month, container, false);


        initCalendarView();
        initListView();

        update();

        return mRootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
        if (mIsTaskFragmentCreated){
            updateTaskListByDate(mSelectedDate);
        }
    }

    @Override
    public void onDayPress(Date date) {
        mSelectedDate = date;
        updateTaskListByDate(date);
    }

    private void updateTaskListByDate(Date date) {
        List<Task> tasks = mDb.getTasksItemsByDay(date);
        mTasksFragment.updateList(tasks, date);
    }

    private void loadHolidays() {
        HolidaysLoadHelper holidaysLoadHelper = new HolidaysLoadHelper(getContext(), this);
        holidaysLoadHelper.loadHolidays();
    }

    @Override
    public void onFinishLoading() {
        update();
    }

    private void initCalendarView() {
        mCalendarView = (CalendarView) mRootView.findViewById(R.id.calendar_view);
        update();
        mCalendarView.setEventHandler(this);
    }

    private void initListView() {
        List<Task> tasksList = new ArrayList<>();

        if (mRootView.findViewById(R.id.fragment_container_month) != null) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            mTasksFragment = new TasksFragment();
            mTasksFragment.setTasks(tasksList);
            mTasksFragment.setLifecycleHandler(new LifecycleHandler() {
                @Override
                public void afterCreate() {
                    updateTaskListByDate(mSelectedDate);
                    mIsTaskFragmentCreated = true;
                }
            });

            fragmentTransaction.add(R.id.fragment_container_month, mTasksFragment);
            fragmentTransaction.commit();

        }
    }

    public void update() {
        HashSet<Date> events = mDb.getAllEventDates();
        mCalendarView.setEventDays(events);
        mCalendarView.updateCalendar();
    }
}

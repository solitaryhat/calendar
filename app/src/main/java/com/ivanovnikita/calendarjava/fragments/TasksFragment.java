package com.ivanovnikita.calendarjava.fragments;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ivanovnikita.calendarjava.R;
import com.ivanovnikita.calendarjava.activities.MainActivity;
import com.ivanovnikita.calendarjava.adapters.TaskRecyclerAdapter;
import com.ivanovnikita.calendarjava.data.Task;
import com.ivanovnikita.calendarjava.interfaces.LifecycleHandler;
import com.ivanovnikita.calendarjava.utils.DateUtils;
import com.ivanovnikita.calendarjava.utils.ViewHelper;

import java.util.Date;
import java.util.List;

public class TasksFragment extends Fragment {
    private View mRootView;
    private List<Task> mTasks;
    private Date mDate;
    private LifecycleHandler mLifecycleHandler;


    public TasksFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_tasks, container, false);
        setupFAB();
        updateList();
        return mRootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mLifecycleHandler.afterCreate();
    }

    public void setTasks(List<Task> tasks) {
        if (tasks.size() > 0) {
            mDate = tasks.get(0).getDate();
        }
        mTasks = tasks;
    }

    public void setLifecycleHandler(LifecycleHandler lifecycleHandler) {
        mLifecycleHandler = lifecycleHandler;
    }

    public void updateList() {
        final RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        TaskRecyclerAdapter taskRecyclerAdapter = new TaskRecyclerAdapter(mTasks);
        taskRecyclerAdapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = recyclerView.getChildLayoutPosition((View)v.getParent());
                MainActivity.showTaskView(ViewHelper.getViewCenterCoord(v),
                        DateUtils.toString(mTasks.get(pos).getDate()), mTasks.get(pos), false);
            }
        });
        recyclerView.setAdapter(taskRecyclerAdapter);
    }

    public void updateList(List<Task> tasks, Date date) {
        mTasks = tasks;
        updateList();
        mDate = date;
    }

    private void setupFAB() {
        final FloatingActionButton fab = (FloatingActionButton) mRootView.findViewById(R.id.fab_add_task);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.showTaskView(ViewHelper.getViewCenterCoord(fab), DateUtils.toString(mDate), null, true);
            }
        });
    }

}

package com.ivanovnikita.calendarjava.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ivanovnikita.calendarjava.Constants;
import com.ivanovnikita.calendarjava.data.Task;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

/**
 * Created by test on 05.05.2016.
 */
public class SQLiteORM extends SQLiteOpenHelper {
    private static final String TABLE_NAME = "holidays";
    private static final String ID = "id";
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String DATE = "date";

    private Context mContext;
    private String mLastDate;
    private SharedPreferences mPreferences;

    public SQLiteORM(Context context) {
        super(context, "HolidaysDataBase", null, 1);
        mContext = context;
        mPreferences =  mContext.getSharedPreferences(Constants.SP_NAME, Context.MODE_PRIVATE);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -2);
        mLastDate = mPreferences.getString(Constants.SP_DATE, DateUtils.toString(calendar.getTime()));
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTableQuery =
                "CREATE TABLE " + TABLE_NAME + "(" +
                        ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                        TITLE + " TEXT," +
                        DESCRIPTION + " TEXT," +
                        DATE + " TEXT)";
        db.execSQL(createTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXIST " + TABLE_NAME);
        onCreate(db);
    }

    public void addTaskItem(Task task) {
        SQLiteDatabase db = getWritableDatabase();
        String dateAsString = DateUtils.toString(task.getDate());
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, task.getTitle());
        contentValues.put(DESCRIPTION, task.getDescription());
        contentValues.put(DATE, dateAsString);
        db.insert(TABLE_NAME, null, contentValues);
        db.close();

        if (DateUtils.before(mLastDate, dateAsString)) {
            mLastDate = dateAsString;
            SharedPreferences.Editor editor = mPreferences.edit();
            editor.putString(Constants.SP_DATE, dateAsString);
            editor.apply();
        }
    }

    public List<Task> getTasksItemsByDay(Date date) {
        String dateAsString = DateUtils.toString(date);
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                TABLE_NAME,
                new String[]{ID, TITLE, DESCRIPTION},
                DATE + "=?",
                new String[]{dateAsString},
                null, null, null, null
        );
        List<Task> tasks = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                tasks.add(new Task(cursor.getString(1), cursor.getString(2), date));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null) {
            cursor.close();
        }
        return tasks;
    }

    public HashSet<Date> getAllEventDates() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                TABLE_NAME,
                new String[]{DATE},
                null,
                null,
                null, null, null, null
        );
        HashSet<Date>  dates = new HashSet<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                dates.add(DateUtils.fromString(cursor.getString(0)));
            } while (cursor.moveToNext());
        }
        db.close();
        if (cursor != null) {
            cursor.close();
        }
        return dates;
    }

    public void deleteAll(){
        SQLiteDatabase database = this.getWritableDatabase();
        database.execSQL("DROP TABLE " + TABLE_NAME);
        this.onCreate(database);
    }

    public void deleteTask(Task task) {
        int id = getTaskId(task);
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_NAME, ID + "=" + id, null);
        database.close();
    }

    private int getTaskId(Task task) {
        String dateAsString = DateUtils.toString(task.getDate());
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(
                TABLE_NAME,
                new String[]{ID, TITLE, DESCRIPTION},
                DATE + "=?",
                new String[]{dateAsString},
                null, null, null, null
        );
        int id = -1;
        List<Task> tasks = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            do {
                tasks.add(new Task(cursor.getString(1), cursor.getString(2), task.getDate()));
                ids.add(Integer.parseInt(cursor.getString(0)));
            } while (cursor.moveToNext());
        }

        for (int i = 0; i < tasks.size(); ++i) {
            if (Task.sameTitleAndDescription(tasks.get(i), task)) {
                id = ids.get(i);
                break;
            }
        }

        db.close();
        if (cursor != null) {
            cursor.close();
        }
        return id;
    }

    public void updateTask(Task oldTask, Task task) {
        int id = getTaskId(oldTask);
        SQLiteDatabase database = this.getWritableDatabase();
        String dateAsString = DateUtils.toString(task.getDate());
        ContentValues contentValues = new ContentValues();
        contentValues.put(TITLE, task.getTitle());
        contentValues.put(DESCRIPTION, task.getDescription());
        contentValues.put(DATE, dateAsString);
        database.update(TABLE_NAME, contentValues, ID + "=" + id, null);
        database.close();
    }

    public String getLastDate() {
        return mLastDate;
    }
}

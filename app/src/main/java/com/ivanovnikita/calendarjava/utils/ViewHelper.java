package com.ivanovnikita.calendarjava.utils;

import android.util.Pair;
import android.view.View;

/**
 * Created by test on 22.05.2016.
 */
public class ViewHelper {

    public static Pair<Integer, Integer> getViewCenterCoord(View view) {
        int[] pos = new int[2];
        view.getLocationOnScreen(pos);
        return new Pair<>(
                (pos[0] + pos[0] + view.getWidth()) / 2,
                (pos[1] + pos[1] + view.getHeight() / 2) / 2
        );
    }
}

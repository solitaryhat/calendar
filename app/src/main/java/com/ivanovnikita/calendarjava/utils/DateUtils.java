package com.ivanovnikita.calendarjava.utils;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by test on 06.05.2016.
 */
public class DateUtils {
    private static final String DATE_FORMAT = "dd-MM-yyyy";

    public static String toString(Date date) {
        return new SimpleDateFormat(DATE_FORMAT).format(date);
    }

    public static Date fromString(String dateAsString) {
        try {
            return new SimpleDateFormat(DATE_FORMAT).parse(dateAsString);
        } catch (ParseException pEx) {
            Log.e("Parse exception", pEx.getMessage(), pEx);
        }
        return null;
    }

    public static Boolean before(String dateString1, String dateString2) {
        Date date1 = fromString(dateString1);
        Date date2 = fromString(dateString2);
        return date1.before(date2);
    }

    public static Boolean sameDate(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    public static Boolean sameMonth(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.MONTH) == cal2.get(Calendar.MONTH);
    }
}

package com.ivanovnikita.calendarjava.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.ivanovnikita.calendarjava.data.Task;
import com.ivanovnikita.calendarjava.interfaces.LoaderHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * Created by test on 07.05.2016.
 */
public class HolidaysLoadHelper {

    private SQLiteORM mDb;
    private JSONArray mHolidaysJson;
    private LoaderHandler mLoaderHandler;

    public HolidaysLoadHelper(Context context, LoaderHandler handler) {
        mDb = new SQLiteORM(context);
        mLoaderHandler = handler;
    }

    public void loadHolidays() {

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 2); // 2 y later

        RequestParams params = new RequestParams();
        params.add("action","getPublicHolidaysForDateRange");
        params.add("fromDate", mDb.getLastDate());
        params.add("toDate", DateUtils.toString(calendar.getTime()));
        params.add("country","rus");
        params.add("region","");
        EnricoRestClient.get("", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray holidays) {
                mHolidaysJson = holidays;

                HolidaysLoader loader = new HolidaysLoader();
                loader.execute();
            }

        });
    }

    private class HolidaysLoader extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                if (mHolidaysJson != null) {
                    for (int i = 0; i < mHolidaysJson.length(); ++i) {
                        JSONObject holiday = new JSONObject(mHolidaysJson.get(i).toString());
                        String title = holiday.getString("englishName");
                        String note = title;
                        if (holiday.has("note")) {
                            note = holiday.getString("note");
                        }
                        JSONObject dateJson = holiday.getJSONObject("date");
                        int day = dateJson.getInt("day");
                        int month = dateJson.getInt("month") - 1;
                        int year = dateJson.getInt("year");

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year,month,day);
                        Date date = new Date(calendar.getTimeInMillis());

                        mDb.addTaskItem(new Task(title, note, date));
                    }

                }
            } catch (JSONException jEx) {
                Log.e("JSONException", jEx.getMessage(), jEx);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mLoaderHandler.onFinishLoading();
        }
    }
}

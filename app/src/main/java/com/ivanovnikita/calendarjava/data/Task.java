package com.ivanovnikita.calendarjava.data;

import java.io.Serializable;
import java.util.Date;

public class Task implements Serializable{
    private String mTitle;
    private String mDescription;

    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        mDate = date;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    private Date mDate;

    public Task(String title, String description, Date date) {
        mTitle = title;
        mDescription = description;
        mDate = date;
    }

    public static Boolean sameTitleAndDescription(Task task1, Task task2) {
        return task1.getTitle().equals(task2.getTitle()) &&
                task1.getDescription().equals(task2.getDescription());
    }
}
